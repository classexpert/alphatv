﻿using System;
using System.Collections.Generic;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace AlphaTV
{
    public static class Info
    {
        private static DispatcherTimer _msgTimer;
        private static Queue<string> _messages;



        private static StatusBar _statusBar
        {
            get { return StatusBar.GetForCurrentView(); }
        }

        public static int Count
        {
            get { return _messages == null ? 0 : _messages.Count; }
        }

        /// <summary>Initialize and Show Status Bar</summary>
        /// <param name=" messageInterval">Interval between messages in queue
        public static void Init(TimeSpan messageInterval)
        {
            _messages = new Queue<string>();
            _msgTimer = new DispatcherTimer();
            _msgTimer.Interval = messageInterval;
            _msgTimer.Tick += _msgTimer_Tick;
            Show();
        }

        /// <summary>Initialize and Show Status Bar with 5 sec interval</summary>
        public static void Init()
        {
            Init(TimeSpan.FromSeconds(5));
            _statusBar.BackgroundColor = Color.FromArgb(100, 45, 72, 133);
        }

        /// <summary>Show CURRENT ProgressIndicator in StatusBar</summary>
        public static async void Show()
        {
            StopProgress();
            await _statusBar.ProgressIndicator.ShowAsync();
        }

        public static void StartIndeterminate()
        {
            _statusBar.ProgressIndicator.ProgressValue = null;
        }

        public static void SetProgress(double val)
        {
            _statusBar.ProgressIndicator.ProgressValue = val;
        }

        /// <summary>Stop any progress</summary>
        public static void StopProgress()
        {
            _statusBar.ProgressIndicator.ProgressValue = 0;
        }

        /// <summary>Add message to messages queue</summary>
        public static void Add(string msg)
        {
            _messages.Enqueue(msg);
            Resume();
        }

        /// <summary>end current info and show other just now</summary>
        public static void QuickInform(string msg)
        {
            _msgTimer.Stop();
            SetText(msg);
            Resume();
        }

        /// <summary>clear messages queue and current info</summary>
        public static void Clear()
        {
            _msgTimer.Stop();
            _messages.Clear();
            SetText(string.Empty);
        }

        /// <summary>set permament text and stop messages queue</summary>
        public static void SetTextAndStop(string text)
        {
            Stop();
            SetText(text);
        }

        /// <summary>stop messages queue</summary>
        public static void Stop()
        {
            _msgTimer.Stop();
        }

        /// <summary>resume messages queue</summary>
        public static void Resume()
        {
            if (!_msgTimer.IsEnabled && _messages.Count > 0)
                _msgTimer.Start();
        }

        /// <summary>set permament operation text and start progress indeterminate</summary>
        public static void StartOperation(string name)
        {
            SetTextAndStop(name);
            StartIndeterminate();
        }

        /// <summary>Stop any progress and resume message queue</summary>
        public static void StopOperation(string result = "")
        {
            StopProgress();
            SetText(result);
            Resume();
        }

        private static void _msgTimer_Tick(object sender, object e)
        {
            if (_messages.Count > 0)
            {
                SetText(_messages.Dequeue());
            }
            else
            {
                _msgTimer.Stop();
                SetText(string.Empty);
            }
        }

        private static void SetText(string text)
        {
            _statusBar.ProgressIndicator.Text = text;
        }
    }
}