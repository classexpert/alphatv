﻿using System.Diagnostics;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using AlphaTV.Common;
using AlphaTV.Entities;

namespace AlphaTV.Views
{
    /// <summary>
    ///     Страница, на которой отображается сгруппированная коллекция элементов.
    /// </summary>
    public sealed partial class MainPageView : Page
    {
        
        public MainPageView()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait | DisplayOrientations.Landscape;  
            InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Required;
            /*
            var nav=new NavigationHelper(this)
            {
                GoBackCommand = new RelayCommand(() =>
                {
                    ChannelManager.Instance.SaveData();
                    Application.Current.Exit();
                })
            };*/
        }

    }
}