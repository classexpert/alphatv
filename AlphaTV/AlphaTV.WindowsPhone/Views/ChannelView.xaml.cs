﻿using System.Diagnostics;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using AlphaTV.Common;
using Microsoft.PlayerFramework;

namespace AlphaTV.Views
{
    /// <summary>
    ///     Страница, на которой отображаются сведения об отдельном элементе внутри группы.
    /// </summary>
    public sealed partial class ChannelView : Page
    {
        public ChannelView()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait | DisplayOrientations.Landscape | DisplayOrientations.LandscapeFlipped; // 
            InitializeComponent();
           
        }
        /*
        protected override void OnDrop(DragEventArgs e)
        {
            Debug.WriteLine("OnDrop");
            base.OnDrop(e);
        }

        protected override void OnHolding(HoldingRoutedEventArgs e)
        {
            Debug.WriteLine("OnHolding");
            base.OnHolding(e);
        }

        protected override void OnKeyUp(KeyRoutedEventArgs e)
        {
            Debug.WriteLine("OnKeyUp");
            base.OnKeyUp(e);
        }

        protected override void OnPointerExited(PointerRoutedEventArgs e)
        {
            Debug.WriteLine("OnPointerExited");
            base.OnPointerExited(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            Debug.WriteLine("OnLostFocus");
            base.OnLostFocus(e);
        }
        

        protected override void OnKeyDown(KeyRoutedEventArgs e)
        {
            Debug.WriteLine("OnKeyDown");
            base.OnKeyDown(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Debug.WriteLine("111");
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Debug.WriteLine("666 " + e.NavigationMode);
            base.OnNavigatedTo(e);
        }

        protected override bool GoToElementStateCore(string stateName, bool useTransitions)
        {
            Debug.WriteLine("9999 " + stateName + " " + useTransitions);
            return base.GoToElementStateCore(stateName, useTransitions);
        }
        */
        
      
    }
}