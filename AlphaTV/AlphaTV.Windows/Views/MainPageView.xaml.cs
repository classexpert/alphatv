﻿// Документацию по шаблону проекта "Универсальное приложение с Hub" см. по адресу http://go.microsoft.com/fwlink/?LinkID=391955
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using AlphaTV.Common;
using AlphaTV.Entities.Parse;

namespace AlphaTV.Views
{
    /// <summary>
    ///     Страница, на которой отображается сгруппированная коллекция элементов.
    /// </summary>
    public sealed partial class MainPageView : Page
    {

        public MainPageView()
        {
            InitializeComponent();

        }

    }
}