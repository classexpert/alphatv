﻿using System.Diagnostics;
using Windows.UI.Xaml;
using Caliburn.Micro;
using Yandex.Metrica;

namespace AlphaTV.ViewModels
{
    public class MainPageViewModel : Conductor<IScreen>.Collection.AllActive
    {
        private readonly HubFavoritesChannelsListViewModel _channelsFavVM;
        private readonly HubChannelsListViewModel _channelsVM;
        private readonly INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService, HubChannelsListViewModel channelsVM,
            HubFavoritesChannelsListViewModel channelsFavVM)
        {
            _navigationService = navigationService;
            Debug.WriteLine("----------------------------------  OnInitialize MainPageViewModel");
            _channelsVM = channelsVM;
            _channelsFavVM = channelsFavVM;
        }

        public void About()
        {
            Debug.WriteLine("About");
            _navigationService.NavigateToViewModel<AboutViewModel>();
        }

        #region overrides

        protected override void OnInitialize()
        {
            base.OnInitialize();
            Debug.WriteLine("OnInitialize");

            Items.Add(_channelsVM);
            Items.Add(_channelsFavVM);
            YandexMetrica.ReportEvent("Main Page");
            //ActivateItem(_channelsVM);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
        }

        protected override void OnViewAttached(object view, object context)
        {
#if WINDOWS_APP
            Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Arrow, 2);
#else
            Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ShowAsync();
#endif
            base.OnViewAttached(view, context);
        }

        #endregion
    }
}