﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using AlphaTV.Entities;
using AlphaTV.Entities.Parse;
using Caliburn.Micro;

namespace AlphaTV.ViewModels
{
    public class HubFavoritesChannelsListViewModel : Screen
    {
        private readonly INavigationService _navigationService;

        public HubFavoritesChannelsListViewModel(INavigationService navigationService)
        {
            Title = "избранное";
            _navigationService = navigationService;
        }

        public string Title { get; set; }

        public ObservableCollection<Channel> Channels
        {
            get { return ChannelManager.Instance.FavoriteChannels; }
            set
            {
                ChannelManager.Instance.FavoriteChannels = value;
                NotifyOfPropertyChange(() => Channels);
            }
        }

        protected override async void OnActivate()
        {
            if (Channels == null)
            {
                Debug.WriteLine("------------------------------- Activated MainPageViewModel");
                Channels = new ObservableCollection<Channel>();
            }
        }

        private void Open(Channel ch)
        {
            _navigationService.NavigateToViewModel<ChannelViewModel>(ch);
        }

        public void ItemClick(object sender, ItemClickEventArgs e)
        {
            var ch = (Channel) e.ClickedItem;
            
            if (ch != null)
                _navigationService.UriFor<ChannelViewModel>().WithParam(x => x.ind, ChannelManager.Instance.FavoriteChannels.IndexOf(ch)).WithParam(x=>x.FromChannels, false).Navigate();
        }
    }
}