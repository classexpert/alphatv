﻿using System;
using System.Diagnostics;

using Caliburn.Micro;
using Yandex.Metrica;

namespace AlphaTV.ViewModels
{
    public class AboutViewModel : Screen
    {
        public async void SendMail()
        {
#if WINDOWS_PHONE_APP
            Debug.WriteLine("SendMail");
            var mail = new Windows.ApplicationModel.Email.EmailMessage();
            mail.Subject = "AlphaTV v.1.2";
            mail.To.Add(new Windows.ApplicationModel.Email.EmailRecipient("alex@codeshake.net"));
            await Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(mail);
#else
            var mailto = new Uri("mailto:alex@codeshake.net");
            await Windows.System.Launcher.LaunchUriAsync(mailto);
#endif
        }

        public async void SendWorkMail()
        {
#if WINDOWS_PHONE_APP
            Debug.WriteLine("SendMail");
            var mail = new Windows.ApplicationModel.Email.EmailMessage();
            mail.Subject = "Предложение о работе";
            mail.To.Add(new Windows.ApplicationModel.Email.EmailRecipient("alex@codeshake.net"));
            await Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(mail);
#else

            var mailto = new Uri("mailto:alex@codeshake.net");
            await Windows.System.Launcher.LaunchUriAsync(mailto);
#endif
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            YandexMetrica.ReportEvent("About Page");
            Debug.WriteLine("OnInitialize  About");
        }
    }
}