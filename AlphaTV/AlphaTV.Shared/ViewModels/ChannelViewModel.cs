﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using AlphaTV.Entities;
using AlphaTV.Entities.Parse;
using AlphaTV.Views;
using Caliburn.Micro;
using Microsoft.PlayerFramework;
using SM.Media.MediaPlayer;
using Yandex.Metrica;

namespace AlphaTV.ViewModels
{
    public class ChannelViewModel : Screen
    {
        public bool FromChannels { get; set; }
        private readonly INavigationService _navigationService;
        private int _ind;
#if WINDOWS_APP
        private bool _controlsIsVisible = true;
#else
        private bool _controlsIsVisible=false;
#endif

        private Channel _parameter;

        public List<Channel> Channels
        {
            get { return ChannelManager.Instance.Channels; }
            set
            {
                ChannelManager.Instance.Channels = value;
                NotifyOfPropertyChange(() => Channels);
            }
        }

        public ObservableCollection<Channel> FavoriteChannels
        {
            get { return ChannelManager.Instance.FavoriteChannels; }
            set
            {
                ChannelManager.Instance.FavoriteChannels = value;
                NotifyOfPropertyChange(() => FavoriteChannels);
            }
        }

        public IList<Channel> CurrentList
        {
            get
            {
                if (SelectedChannel != null)
                    return Channels;
                return FavoriteChannels;
            }
        }

        public ChannelViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            _navigationService.Navigating += _navigationService_Navigating;
            starTimer.Interval = TimeSpan.FromSeconds(1.5);
            starTimer.Tick += starTimer_Tick;
#if WINDOWS_PHONE_APP
            Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();
#endif
        }

        void starTimer_Tick(object sender, object e)
        {
            Debug.WriteLine("----------------- Timer Tick");
           starTimer.Stop();
           Stream = null;
           Stream = Parameter.Stream;
        }

        void _navigationService_Navigating(object sender, Windows.UI.Xaml.Navigation.NavigatingCancelEventArgs e)
        {
            Debug.WriteLine(e.NavigationMode);
        }

        public MediaPlayer Player { get; set; }

        public ListView ChannelsListView { get; set; }

        public ListView FavChannelsListView { get; set; }

        public StreamingMediaPlugin StreamPlugin { get; set; }

        private int? _indexOfFavorite = null;

        protected int? IndexOfFavorite
        {
            get { return _indexOfFavorite; }
            set
            {
                _indexOfFavorite = value; NotifyOfPropertyChange(() => IsFavorite);
                NotifyOfPropertyChange(() => IsFavorite);
                NotifyOfPropertyChange(() => IsNotFavorite);
            }
        }
        public bool IsFavorite
        {
            get
            {
                if (_indexOfFavorite == null && Parameter != null)
                {
                    var item = ChannelManager.Instance.FavoriteChannels.FirstOrDefault(e => e.Id == Parameter.Id);
                    _indexOfFavorite = item != null ? ChannelManager.Instance.FavoriteChannels.IndexOf(item) : -1;
                }
                return _indexOfFavorite != -1;
            }
        }

        public bool IsNotFavorite
        {
            get
            {
                if (_indexOfFavorite == null && Parameter!=null)
                {
                    var item = ChannelManager.Instance.FavoriteChannels.FirstOrDefault(e => e.Id == Parameter.Id);
                    _indexOfFavorite = item != null ? ChannelManager.Instance.FavoriteChannels.IndexOf(item) : -1;
                }
                return _indexOfFavorite == -1;
            }
        }

        public Channel Parameter
        {
            get { return _parameter; }
            set
            {
                _parameter = value;
                NotifyOfPropertyChange(() => Parameter);
                starTimer.Start();
                IndexOfFavorite = null;
            }
        }

        private Channel _seleChannel;

        public Channel SelectedChannel
        {
            get { return _seleChannel; } set { _seleChannel = value; NotifyOfPropertyChange(()=> SelectedChannel); }
        }

        private Channel _seleFavChannel;
        public Channel SelectedFavChannel
        {
            get { return _seleFavChannel; }
            set { _seleFavChannel = value; NotifyOfPropertyChange(() => SelectedFavChannel); }
        }

        DispatcherTimer starTimer = new DispatcherTimer();

        public bool ControlsIsVisible
        {
            get { return _controlsIsVisible; }
            set
            {
                _controlsIsVisible = value;
                NotifyOfPropertyChange(() => ControlsIsVisible);
#if WINDOWS_APP
                if (_controlsIsVisible)
                    Window.Current.CoreWindow.PointerCursor =  new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Arrow, 2);

                else
                {
                    Window.Current.CoreWindow.PointerCursor = null;
                }
#endif
            }
        }

        public bool CanGoBack
        {
            get { return ind > 0; }
        }

        public bool CanGoForward
        {
            get { return ind < CurrentList.Count - 1; }
        }

        public int ind
        {
            get { return _ind; }
            set
            {
                _ind = value;
                NotifyOfPropertyChange(() => ind);
                NotifyOfPropertyChange(() => CanGoBack);
                NotifyOfPropertyChange(() => CanGoForward);
            }
        }

        public void ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            double horizontalVelocity = e.Velocities.Linear.X;
            Debug.WriteLine(horizontalVelocity);
            if (Math.Abs(horizontalVelocity) > 600)
            {
                if (horizontalVelocity < 0)
                    Back();
                else
                    Next();
            }
        }

        public void Back()
        {
            if (Player != null)
                Player.IsInteractiveChanged -= IsInteractiveChanged;
            _navigationService.GoBack();
        }

        public void Prev()
        {
            if (CanGoBack)
            {
                starTimer.Stop();
                _ind--;
                UpdateChannel();
                YandexMetrica.ReportEvent("Prev");
            }
        }

        private void UpdateChannel()
        {
            if (SelectedChannel != null)
            {
                Parameter = ChannelManager.Instance.Channels[ind];
                SelectedChannel = Parameter;
                if(ChannelsListView!=null)
                    ChannelsListView.ScrollIntoView(Parameter, ScrollIntoViewAlignment.Leading);
            }
            else
            {
                Parameter = ChannelManager.Instance.FavoriteChannels[ind];
                SelectedFavChannel = Parameter;
                if (FavChannelsListView != null)
                    FavChannelsListView.ScrollIntoView(Parameter, ScrollIntoViewAlignment.Leading);
            }
            NotifyOfPropertyChange(() => CanGoBack);
            NotifyOfPropertyChange(() => CanGoForward);
        }
        public void Next()
        {
            if (CanGoForward)
            {
                starTimer.Stop();
                _ind++;
                UpdateChannel();
                YandexMetrica.ReportEvent("Next");
            }
        }

        private string _stream = null;
        public string Stream { get { return _stream ;}  set { _stream = value; NotifyOfPropertyChange(()=>Stream); }}
        public void Like()
        {
            if (SelectedChannel != null)
            {
                ChannelManager.Instance.FavoriteChannels.Add(Parameter);
                YandexMetrica.ReportEvent("Like " + Parameter.Title);
                _indexOfFavorite = ChannelManager.Instance.FavoriteChannels.Count - 1;

                NotifyOfPropertyChange(() => IsFavorite);
                NotifyOfPropertyChange(() => IsNotFavorite);
            }
        }

        public void Unlike()
        {
            if (SelectedFavChannel != null)
            {
                YandexMetrica.ReportEvent("Unlike " + Parameter.Title);
                ind = Channels.FindIndex(e => e.Id == SelectedFavChannel.Id);
                FavoriteChannels.Remove(SelectedFavChannel);
                SelectedChannel = Parameter;
                UpdateChannel();
                NotifyOfPropertyChange(() => CanGoBack);
                NotifyOfPropertyChange(() => CanGoForward);
            }
            else
            {
                FavoriteChannels.RemoveAt(_indexOfFavorite.Value);

                _indexOfFavorite = -1;
                NotifyOfPropertyChange(() => IsFavorite);
                NotifyOfPropertyChange(() => IsNotFavorite);
            }
        }

        public void IsInteractiveChanged(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("11");
            if (Player != null)
                ControlsIsVisible = Player.IsInteractive;
       
        }

        public void DisableInteraction()
        {
            Debug.WriteLine("DisableInteraction");
            if(Player!=null)
                Player.InteractiveDeactivationMode = InteractionType.None;    
        }

        public void EnableInteraction()
        {
            Debug.WriteLine("EnableInteraction");
            if (Player != null)
                Player.InteractiveDeactivationMode = InteractionType.All;
        }

        public void ShowControls()
        {
            Player.IsInteractive = true;
        }
        
        public void ItemChannelClick()
        {
            if (SelectedChannel!=null)
            {
                SelectedFavChannel = null;
                ind = ChannelManager.Instance.Channels.IndexOf(SelectedChannel);
                Parameter = SelectedChannel;
                ChannelsListView.ScrollIntoView(Parameter);
                YandexMetrica.ReportEvent("Click " + Parameter.Title);
            }
        }

        public void ItemFavChannelClick()
        {
            if (SelectedFavChannel != null)
            {
                SelectedChannel = null;
                ind = ChannelManager.Instance.FavoriteChannels.IndexOf(SelectedFavChannel);
                Parameter = SelectedFavChannel;
                FavChannelsListView.ScrollIntoView(Parameter);
                YandexMetrica.ReportEvent("Favorite_Click " + Parameter.Title);
            }
        }

        protected override void OnActivate()
        {
            Debug.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~ " + ind + " " + FromChannels);
            Player = (GetView() as Page).FindName("Player") as MediaPlayer; // Looking so bad
            ChannelsListView = (GetView() as Page).FindName("ChannelsListView") as ListView;
            FavChannelsListView = (GetView() as Page).FindName("FavChannelsListView") as ListView;
#if WINDOWS_PHONE_APP
            Player.IsInteractive = true;
#endif
            if (Player != null)
            {
                Player.IsInteractive = true;
                Player.IsInteractiveChanged += IsInteractiveChanged;
            }
        }

        protected override void OnViewLoaded(object view)
        {
            if (FromChannels)
            {
                SelectedChannel = Channels[ind];
                YandexMetrica.ReportEvent("Main_Click " + SelectedChannel.Title);
            }
            else
            {
                SelectedFavChannel = FavoriteChannels[ind];
                YandexMetrica.ReportEvent("Main_Click_Favorite " + SelectedFavChannel.Title);
            }
            UpdateChannel();
            Debug.WriteLine(Parameter.Stream + " " + ind);
            YandexMetrica.ReportEvent("Channel Page");
            base.OnViewLoaded(view);
        }
    }
}