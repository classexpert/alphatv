﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using AlphaTV.Entities;
using AlphaTV.Entities.Parse;
using AlphaTV.Services;
using Caliburn.Micro;

namespace AlphaTV.ViewModels
{
    public class HubChannelsListViewModel : Screen
    {
        private readonly INetParser _parseService;
        private readonly INavigationService _navigationService;
        private bool _isInited = false;
        public HubChannelsListViewModel(INavigationService navigationService, INetParser parseService)
        {
            Title = "список";
            _navigationService = navigationService;
            _parseService = parseService;
        }

        public string Title { get; set; }
        public List<Channel> Channels
        {
            get { return ChannelManager.Instance.Channels; }
            set
            {
                ChannelManager.Instance.Channels = value;
                NotifyOfPropertyChange(() => Channels);
            }
        }

        protected override async void OnActivate()
        {
            Debug.WriteLine("------------------------------- Activated MainPageViewModel");
            if (Channels == null || !_isInited)
            {
                try
                {
                    _isInited = true;
                    var res = await _parseService.GetChannels();
                    int hash = res.GetHashCode();
                    
                    Debug.WriteLine("Channels HASH " + hash);
                    if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("ListHash") ||
                        (int) ApplicationData.Current.LocalSettings.Values["ListHash"] != hash)
                    {
                        Channels = res;
                        ChannelManager.Instance.UpdateFavoriteStreams();
                    }

                    ApplicationData.Current.LocalSettings.Values["ListHash"] = hash;
                }
                catch (Exception)
                {
                   
                }
            }
        }

        private void Open(Channel ch)
        {
            _navigationService.NavigateToViewModel<ChannelViewModel>(ch);
        }

        public void ItemClick(object sender, ItemClickEventArgs e)
        {
            var ch = (Channel) e.ClickedItem;
            if(ch!=null)
                _navigationService.UriFor<ChannelViewModel>().WithParam(x => x.ind, ChannelManager.Instance.Channels.IndexOf(ch)).WithParam(x=>x.FromChannels, true).Navigate();
        }
    }
}