﻿// Документацию по шаблону проекта "Универсальное приложение с Hub" см. по адресу http://go.microsoft.com/fwlink/?LinkID=391955
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using AlphaTV.Entities;
using AlphaTV.Services;
using AlphaTV.ViewModels;
using AlphaTV.Views;
using Caliburn.Micro;
using Yandex.Metrica;
using Windows.UI.Xaml.Media;
using Windows.UI;

namespace AlphaTV
{
    /// <summary>
    ///     Обеспечивает зависящее от конкретного приложения поведение, дополняющее класс Application по умолчанию.
    /// </summary>
    public sealed partial class App
    {
        private WinRTContainer container;

        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            container = new WinRTContainer();

            container.RegisterWinRTServices();

            container.PerRequest<MainPageViewModel>();
            container.PerRequest<HubChannelsListViewModel>();
            container.PerRequest<HubFavoritesChannelsListViewModel>();
            container.PerRequest<AboutViewModel>();
            container.PerRequest<ChannelViewModel>();
            container.PerRequest<INetParser, NetParser>();
#if WINDOWS_APP
            var viewSettings = new Dictionary<string, object>();
            viewSettings["Background"] = new SolidColorBrush(Colors.Black);
            container.RegisterSettingsService().RegisterFlyoutCommand<AboutViewModel>("О программе", viewSettings);
#endif
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            container.RegisterNavigationService(rootFrame);
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            await ChannelManager.Instance.LoadData();
            DisplayRootView<MainPageView>();
#if WINDOWS_APP
            YandexMetrica.Start(40904);
#else
            YandexMetrica.Start(40916);
#endif
        }

        protected override void OnSuspending(object sender, SuspendingEventArgs e)
        {
            Debug.WriteLine("------------------------ SUSPEND");
            ChannelManager.Instance.SaveData();
            YandexMetrica.ReportExit();
            base.OnSuspending(sender, e);
        }


        protected override void PrepareApplication()
        {
            Debug.WriteLine("------------------------ PrepareApplication");
            base.PrepareApplication();
        }

        protected override void OnCachedFileUpdaterActivated(CachedFileUpdaterActivatedEventArgs args)
        {
            Debug.WriteLine("------------------------ OnCachedFileUpdaterActivated");
            base.OnCachedFileUpdaterActivated(args);
        }


        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }


    }
}