﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using AlphaTV.Entities.Parse;
using AlphaTV.Extensions;
using Newtonsoft.Json;

namespace AlphaTV.Entities
{
    public class ChannelManager
    {
        private static ChannelManager _instance;

        private ChannelManager()
        {
        }

        public static ChannelManager Instance
        {
            get { return _instance ?? (_instance = new ChannelManager()); }
        }

        public List<Channel> Channels { get; set; }

        public ObservableCollection<Channel> FavoriteChannels { get; set; }

        public void SaveData()
        {
            SaveData(FavoriteChannels, "Fav.xml");
            SaveData(Channels, "Channels.xml");
        }
        private void SaveData(object data, string key)
        {
            Debug.WriteLine("SaveData " + key);
            var res = data.Serialize();
            var t = ApplicationData.Current.LocalFolder.CreateFileAsync(key, CreationCollisionOption.ReplaceExisting);
            t.AsTask().Wait();
            StorageFile sampleFile = t.GetResults();
            FileIO.WriteTextAsync(sampleFile, res).AsTask().Wait();
        }

        public async Task LoadData()
        {
            Debug.WriteLine("LoadData 1");
            if (Channels == null || FavoriteChannels == null)
            {
                Debug.WriteLine("LoadData 2");
                try
                {
                    StorageFile fav = await ApplicationData.Current.LocalFolder.GetFileAsync("Fav.xml");
                    var favres = await FileIO.ReadTextAsync(fav);
                    FavoriteChannels = favres.Deserialize<ObservableCollection<Channel>>();

                    StorageFile ch = await ApplicationData.Current.LocalFolder.GetFileAsync("Channels.xml");
                    var chres = await FileIO.ReadTextAsync(ch);
                    Channels = chres.Deserialize<List<Channel>>();
                    //SaveData();
                }
                catch (Exception ex)
                {
                    FavoriteChannels = new ObservableCollection<Channel>();
                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    var chans = loader.GetString("Channels");
                    Channels = JsonConvert.DeserializeObject<List<Channel>>(chans);
                    Debug.WriteLine("---------------- Settings Is Empty " + ex.Message + " " + ex.StackTrace);

                }

            }
        }

        public void UpdateFavoriteStreams()
        {
            var unexists = new List<Channel>();
            foreach (var favoriteChannel in FavoriteChannels)
            {
                var channel = Channels.FirstOrDefault(e => e.Id == favoriteChannel.Id);
                if (channel != null)
                {
                    if(channel.Stream != favoriteChannel.Stream)
                        favoriteChannel.Stream = channel.Stream;
                }
                else
                {
                    unexists.Add(favoriteChannel);
                }
            }

            if(unexists.Count>0)
                FavoriteChannels=new ObservableCollection<Channel>(FavoriteChannels.Except(unexists));
        }

    }
}