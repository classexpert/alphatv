﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AlphaTV.Entities.Parse
{
    [DataContract]
    public class Channel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string Info { get; set; }
        [DataMember]
        public string Stream { get; set; }

    }

}
