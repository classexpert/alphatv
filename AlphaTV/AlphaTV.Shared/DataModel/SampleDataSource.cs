﻿// Модель данных, определяемая этим файлом, служит типичным примером строго типизированной
// по умолчанию.  Имена свойств совпадают с привязками данных из стандартных шаблонов элементов.
//
// Приложения могут использовать эту модель в качестве начальной точки и добавлять к ней дополнительные элементы или полностью удалить и
// заменить ее другой моделью, соответствующей их потребностям. Использование этой модели позволяет повысить качество приложения 
// скорость реагирования, инициируя задачу загрузки данных в коде программной части для App.xaml, если приложение 
// запускается впервые.
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Storage;
using AlphaTV.Entities.Parse;
using Newtonsoft.Json;

namespace AlphaTV.Data
{
    public sealed class SampleDataSource
    {
        private static SampleDataSource _sampleDataSource = new SampleDataSource();

        private readonly ObservableCollection<Channel> _groups = new ObservableCollection<Channel>();

        public ObservableCollection<Channel> Groups
        {
            get { return _groups; }
        }


        private async Task GetSampleDataAsync()
        {
            if (_groups.Count != 0)
                return;

            var dataUri = new Uri("ms-appx:///DataModel/SampleData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);

            var deserialized = JsonConvert.DeserializeObject<RootObject>(jsonText);
            foreach (Channel item in deserialized.channel)
            {
                _groups.Add(item);
            }
        }
    }
}