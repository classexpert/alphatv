﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Web.Http;
using Newtonsoft.Json;
using AlphaTV.Entities.Parse;

namespace AlphaTV.Services
{
    public class NetParser : INetParser
    {
        public Task<List<Channel>> GetChannels()
        {
            return GetChannels(new Uri("http://tv.codeshake.net/api/Channels"));
        }

        public async Task<List<Channel>> GetChannels(Uri url)
        {
            var client = new HttpClient();
            using (var response = await client.GetAsync(url))
            {
                Debug.WriteLine("------------------------------- " + await response.Content.ReadAsStringAsync());
                Debug.WriteLine(response.StatusCode);
                var deserialized = JsonConvert.DeserializeObject<List<Channel>>(await response.Content.ReadAsStringAsync());
                Debug.WriteLine("SUCC");
                deserialized.Sort(
                    (a, b) =>
                    {
                        if (ContainsRussianChars(a.Title) && !ContainsRussianChars(b.Title))
                            return -1;
                        if (!ContainsRussianChars(a.Title) && ContainsRussianChars(b.Title))
                            return 1;

                        return System.String.Compare(a.Title, b.Title, StringComparison.CurrentCultureIgnoreCase);
                    }
                    );
                return deserialized;
            }

            return null;
        }

        private bool ContainsRussianChars(string s)
        {
            return s.Any(c => (c > 'а' && c < 'я') || (c > 'А' && c < 'Я'));
        }
    }
}
