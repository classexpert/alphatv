﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaTV.Entities.Parse;

namespace AlphaTV.Services
{
    public interface INetParser
    {
        Task<List<Channel>> GetChannels();
        Task<List<Channel>> GetChannels(Uri url);
    }
}