﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlphaTV
{
    public class MediaTrack
    {
        public Uri Url { get; set; }
        public string Title { get; set; }
    }

    static class TvManager
    {
        static readonly MediaTrack[] Sources =
        {
            new MediaTrack
            {
                Title = "NASA TV",
                Url = new Uri("http://hls.novotelecom.ru/streaming/tvc/tvrec/playlist.m3u8")
            },
            new MediaTrack
            {
                Title = "NPR",
                Url = new Uri("http://www.npr.org/streams/mp3/nprlive24.pls")
            },
            new MediaTrack
            {
                Title = "Bjarne Stroustrup - The Essence of C++",
                Url = new Uri("http://media.ch9.ms/ch9/ca9a/66ac2da7-efca-4e13-a494-62843281ca9a/GN13BjarneStroustrup.mp3"),
            },
            new MediaTrack
            {
                Title = "Apple",
                Url = new Uri("http://hls.novotelecom.ru/streaming/vesti/tvrec/playlist.m3u8")
            },
            null,
            new MediaTrack
            {
                Title = "Apple 16x9",
                Url = new Uri("https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8")
            }
        };

        public static IList<MediaTrack> Tracks
        {
            get { return Sources; }
        }
    }
}
